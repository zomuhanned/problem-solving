<?php
class Solution {
/** Description
 * An integer is a palindrome when it reads the same backward as forward.
 */
/** Example
 * Input: x = 121
 * Output: true
 */
    /**
     * @param Integer $x
     * @return Boolean
     */
    function isPalindrome($x) {
        $num = str_split(strval($x));
        $num = array_reverse($num);
        $num = implode("",$num);
        
        if ($num != $x){
            return false;
        }
       
        return true;
        
    }
}
<?php
class Solution {

    /**
     * @param Integer[] $changed
     * @return Integer[]
     */
    function findOriginalArray($changed) {
        $original = array();
        $mults = array();
        $valid = count($changed)/2;
        foreach($changed as $key => $element){
            if($element == 0 || !in_array($element, $original)){
              $mul = $element*2;
               
                $k = array_search($mul,$changed);
              if(in_array($mul,$changed) && $key != $k && !in_array($key, $mults)){
             
                    array_push($original,$element);
                    array_push($mults,$k);
                    unset($changed[$key]); unset($changed[$k]);
                    
                }
                
            }
        }
        
        if(count($original) != $valid){
            return [];
        }
        
        return $original;
        
    }
}
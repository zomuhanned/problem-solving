<?php
class Solution {
/** Description
 *  Given a string containing just the characters '(' and ')', find the length of the longest valid (well-formed) parentheses substring.
 */    
/** Example
 * Input: s = ")()())"
 * Output: 4
 * */    
    /**
     * @param String $s
     * @return Integer
     */
    function longestValidParentheses($s) {
        $arr = [-1];
        $result = 0;
        
        for($i=0; $i< strlen($s); $i++){
            $top = end($arr);
            if($top != -1 && $s[$i] == ")" && $s[$top] == "("){
                array_pop($arr);
                $result = max($result, $i - end($arr));
            }else{
                array_push($arr, $i);
            }
        }
        return $result;
    }
}
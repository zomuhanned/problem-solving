<?php
class Solution {
/** Description
 * A pangram is a sentence where every letter of the English alphabet appears at least once.
 * Given a string sentence containing only lowercase English letters, return true if sentence is a pangram, or false otherwise.
 */
/** Example
 * Input: sentence = "thequickbrownfoxjumpsoverthelazydog"
 * Output: true
 * Explanation: sentence contains at least one of every letter of the English alphabet.
 */
    /**
     * @param String $sentence
     * @return Boolean
     */
    function checkIfPangram($sentence) {
        $alphas = range('a', 'z');
        
        for($i = 0; $i < count($alphas); $i++){
            if(strpos($sentence,$alphas[$i]) === false){
                return false;
            }
        }
        return true;
    }
}
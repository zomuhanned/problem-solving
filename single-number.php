<?php
class Solution {
/** Description
 * Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.
 */
/** Example
 * Input: nums = [4,1,2,1,2]
 * Output: 4
 */
    /**
     * @param Integer[] $nums
     * @return Integer
     */
    function singleNumber($nums) {
        $counts = array_count_values($nums);
        if(count($nums) == 1){
            return $nums[0];
        }
        foreach($nums as $element){
            if($counts[$element] === 1){
                return $element;
            }
        }
    }
}
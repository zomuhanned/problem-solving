<?php
class Solution {
/**
 * Given a string s, reverse the string according to the following rules:
 * All the characters that are not English letters remain in the same position.
 * All the English letters (lowercase or uppercase) should be reversed.
 * Return s after reversing it.
 */
/**
 * Input: s = "a-bC-dEf-ghIj"
 * Output: "j-Ih-gfE-dCba"
 */
    /**
     * @param String $s
     * @return String
     */
    function reverseOnlyLetters($s) {
        $l = 0;
        $r = strlen($s);

        while ($l < $r) {
            while ($l < $r && !ctype_alpha($s[$l])) {
                $l++;
            }
            while ($l < $r && !ctype_alpha($s[$r])) {
                $r--;
            }
            if($s[$l] == "" && $s[$r] == ""){
                return $s;
            }
            [$s[$l], $s[$r]] = [$s[$r], $s[$l]];
            $l++;
            $r--;
        }

        return $s;
    }
}
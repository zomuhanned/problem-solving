<?php
class Solution {
/** Description
 * Write an algorithm to determine if a number n is happy.

 * A happy number is a number defined by the following process:

 * Starting with any positive integer, replace the number by the sum of the squares of its digits.
 * Repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1.
 * Those numbers for which this process ends in 1 are happy.
 * Return true if n is a happy number, and false if not.
 */
/** Example
 * Input: n = 19
 * Output: true
 * Explanation:
 * 12 + 92 = 82
 * 82 + 22 = 68
 * 62 + 82 = 100
 * 12 + 02 + 02 = 1
 */
    /**
     * @param Integer $n
     * @return Boolean
     */

    function isHappy($n) {

         $result = 0;
         $arr = array();
         $num = str_split($n);
        do{
           for($i = 0; $i<count($num); $i++){
               
               $result = 0 + $num[$i] * $num[$i];
           }
            
           if(in_array($result, $arr)){
               return false;
           } 
           array_push($arr, $result);
           $num = str_split($result);
        }while($result != 1);
        
        return true;    
        
    }
}
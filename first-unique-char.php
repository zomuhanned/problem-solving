<?php
class Solution {
/** Description
 * Given a string s, find the first non-repeating character in it and return its index. If it does not exist, return -1.
 */
/** Example
 * Input: s = "loveleetcode"
 * Output: 2
 */
    /**
     * @param String $s
     * @return Integer
     */
    function firstUniqChar($s) {
        
        for($i = 0; $i < strlen($s); $i++){
            $count = substr_count($s, $s[$i]);
            if($count == 1){
                return $i;
            }
        }
        
        return -1;
        
        
    }
}
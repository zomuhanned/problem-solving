<?php

class Solution{
/** Description
 * Have the function FirstReverse(str) take the str parameter being passed and return the string in reversed order
 */
/**Example
 * Input: "coderbyte"
   Output: etybredoc
 */
    function FirstReverse($str) {

        // code goes here
          $len = strlen($str);
        
          if($len == 1){
              return $str;
          }
          else{
              $len--;
                
              return FirstReverse(substr($str,1, $len)) 
                              . substr($str, 0, 1);
          }
    }
}
<?php

class Solution{
/** Example 
 * array("(1,2)", "(2,4)", "(7,2)") true
 */

    function TreeConstructor($strArr) {

        foreach ($strArr as $value){
              $result = str_replace('(', '', $value);
              $result = str_replace(')', '', $result);
              
              $parents[$result[2]][] = $result[0];// push all children to his parents
              if (count($parents[$result[2]]) > 2)//if parent has more than 2 children will return false
                  return 'false';
              $children[$result[0]][] = $result[2];// push all parents to his children
            
              if (count($children[$result[0]]) > 1)//if children has more than 1 parent will return false
                  return 'false';
      
          }
          return 'true';
      
    }
}
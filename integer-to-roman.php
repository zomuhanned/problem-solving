<?php
class Solution {
/** Description
 * Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
 * Symbol       Value
 * I             1
 * V             5
 * X             10
 * L             50
 * C             100
 * D             500
 * M             1000
 */
/** Example
 * Input: num = 58
 * Output: "LVIII"
 * Explanation: L = 50, V = 5, III = 3.
 */
    /**
     * @param Integer $num
     * @return String
     */
    function intToRoman($num) {
        $result = "";
        $int = [1000,900,500,400,100,90,50,40,10,9,5,4,1];
        $roman = ["M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"];
        
        for($i = 0; $i < count($int); $i++){
            while($num >= $int[$i]){
                $result .= $roman[$i];
                $num -= $int[$i];
            }
        }
        
        return $result;
    }
}
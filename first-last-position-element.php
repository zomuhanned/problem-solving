<?php
class Solution {
/** Description
 * Given an array of integers nums sorted in non-decreasing order, find the starting and ending position of a given target value.

 * If target is not found in the array, return [-1, -1].
 */
/** Example
 * Input: nums = [5,7,7,8,8,10], target = 8
 * Output: [3,4]
 */
    /**
     * @param Integer[] $nums
     * @param Integer $target
     * @return Integer[]
     */
    function searchRange($nums, $target) {
        $result = array();
        $first = -1;
        $last = -1;
        
        if(!in_array($target,$nums)){
            return [-1,-1];
        }else{
            
            $first = array_search($target, $nums);
            $last = array_search($target, $nums);
            array_push($result, $first);
                for($i = 0; $i < count($nums); $i++){
                    if($nums[$i] == $target && $i > $last){
                        $last = $i;
                    }
                }
        }
        array_push($result, $last);
        return $result;
    }
    
}
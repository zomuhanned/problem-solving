<?php
/**
 * Definition for a binary tree node.
 * class TreeNode {
 *     public $val = null;
 *     public $left = null;
 *     public $right = null;
 *     function __construct($val = 0, $left = null, $right = null) {
 *         $this->val = $val;
 *         $this->left = $left;
 *         $this->right = $right;
 *     }
 * }
 */
class Solution {
/**
 * Given the root of a binary tree, return its maximum depth.
 * A binary tree's maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.
 */
/**
 * Input: root = [3,9,20,null,null,15,7]
 * Output: 3
 */
    /**
     * @param TreeNode $root
     * @return Integer
     */
    function maxDepth($root) {
        $sol = 0;
        
        if($root == null) {
            return $sol;
        }
        
        $leftDepth = $root->left ? $this->maxDepth($root->left) : 0;
        $rightDepth = $root->right ? $this->maxDepth($root->right) : 0;
        
        if($leftDepth > $rightDepth){
            $sol = 1 + $leftDepth;
        }else{
            $sol = 1 + $rightDepth;   
        }
        return $sol;
    }
}

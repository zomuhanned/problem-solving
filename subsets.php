<?php

class Solution {
/** Description
 * Given an integer array nums of unique elements, return all possible subsets (the power set).

 * The solution set must not contain duplicate subsets. Return the solution in any order.
 */
/** Example
 * Input: nums = [1,2,3]
 * Output: [[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
 */
    /**
     * @param Integer[] $nums
     * @return Integer[][]
     */
    function subsets($nums) {
        
        $result = [[]];
        foreach($nums as $element){
            foreach($result as $comp){
                $result[] = array_merge($comp, array($element));
            }   
        }
        
        return $result;
    }
}
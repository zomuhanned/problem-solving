<?php
class Solution {
/** Description
 * Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

 * An input string is valid if:

 * Open brackets must be closed by the same type of brackets.
 * Open brackets must be closed in the correct order.
 * Every close bracket has a corresponding open bracket of the same type.
 */
/** Example
 * Input: s = "()[]{}"
 * Output: true
 */
    /**
     * @param String $s
     * @return Boolean
     */
    function isValid($s) {
        $arr = array();
        
        for($i = 0; $i < strlen($s); $i++){
            if(($s[$i] == "(") || ($s[$i] == "{") || ($s[$i] == "[")){
                array_push($arr, $s[$i]);
            }else{
                if(count($arr) == 0){
                    return false;
                }
                $pop = array_pop($arr);
                if($pop == "(" && $s[$i] == ")"){
                    continue;
                }elseif($pop == "[" && $s[$i] == "]"){
                    continue;
                }elseif($pop == "{" && $s[$i] == "}"){
                    continue;
                }else{
                    return false;
                }
            }
        }
        return count($arr) == 0? true : false;
    }
}
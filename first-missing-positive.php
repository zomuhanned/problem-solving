<?php 
class Solution {
/** Description
 * Given an unsorted integer array nums, return the smallest missing positive integer.
 */
/** Example
 * Input: nums = [1,2,0]
 * Output: 3
 */
    /**
     * @param Integer[] $nums
     * @return Integer
     */
    function firstMissingPositive($nums) {
        $result = 1;
        
        sort($nums);
        
        for($i = 0; $i < count($nums); $i++){
            if(($nums[$i+1] != $nums[$i] + 1) && $nums[$i] + 1 > 0 && $nums[$i+1] != $nums[$i]){
                if(!in_array(1,$nums)){
                    return $result;
                }else{
                    $result = $nums[$i] + 1;
                    return $result;
                }
            }
        }
        return $result; 
    }
}
<?php
class Solution {
/**Description
 * Given an integer array nums, return true if there exists a triple of indices (i, j, k) 
 * such that i < j < k and nums[i] < nums[j] < nums[k]. 
 * If no such indices exists, return false. 
 **/
/**
 * Input: nums = [1,2,3,4,5]
 * Output: true
 * Explanation: Any triplet where i < j < k is valid.
 **/
    /**
     * @param Integer[] $nums
     * @return Boolean
     */
    function increasingTriplet($nums) {
        
        $left = PHP_INT_MAX;
        $mid = PHP_INT_MAX;
        if(count($nums) < 3){
            return false;
        }
        
        for($i = 0; $i < count($nums); $i++){
        
            if($nums[$i] < $left){
                $left = $nums[$i];
            }elseif($nums[$i] > $left && $nums[$i] < $mid){
                $mid = $nums[$i];
            }elseif($nums[$i] > $mid){
                return "true";
            }
            
            
        }
        return false;
    }
}
<?php
class Solution {
/** Description
 * Given a sorted array of distinct integers and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.
 */
/** Example
 * Input: nums = [1,3,5,6], target = 2
 * Output: 1
 */
    /**
     * @param Integer[] $nums
     * @param Integer $target
     * @return Integer
     */
    function searchInsert($nums, $target) {
        if($target < $nums[0]){
            return 0;
        }elseif($target > $nums[count($nums) - 1]){
            return count($nums);
        }else{
            if(in_array($target,$nums)){
               return array_search($target,$nums); 
            }else{
                for($i = 0; $i < count($nums); $i++){
                    if($target > $nums[$i] && $target < $nums[$i + 1]){
                        return $i + 1;
                    }
                }
            }
        }
    }
}
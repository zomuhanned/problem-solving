<?php 
class Solution {
/**Example
 * Input: input = "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext"
   Output: 20
 */
    /**
     * @param String $input
     * @return Integer
     */
    function lengthLongestPath($input) {
        $lines = explode("\n", $input);
        $map = array_fill(0, count($lines), 0);
        $result = 0;
        foreach ($lines as $line) {
            $level = substr_count($line, "\t");
            $tar_name = substr($line, $level);
            
            if (str_contains($tar_name, ".")) {
                $result = max($result, $map[$level - 1] + strlen($tar_name));
            } else {
                $map[$level] = $map[$level - 1] + 1 + strlen($tar_name);
            }
        }
        
        return $result;
    }
}
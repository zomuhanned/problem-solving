<?php

class Solution {
/** Description
 * Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.
*/
/** Example
 * Input: nums1 = [1,2], nums2 = [3,4]
 *Output: 2.50000
*/

    /**
     * @param Integer[] $nums1
     * @param Integer[] $nums2
     * @return Float
     */
    function findMedianSortedArrays($nums1, $nums2) {
        $result = array();
        $result = array_merge($nums1,$nums2);
        sort($result);
        $lenght = count($result);
        $rest = $lenght % 2;
        $middle_index = $lenght / 2;
        if($rest == 0){
            $median = ($result[$middle_index - 1] + $result[$middle_index])/2;
        }else{
            $median = $result[$middle_index];
        }
        return $median;
        
    }
}